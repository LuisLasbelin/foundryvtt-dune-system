export class DuneRoll {
  async performTest(dicePool, target, focus, usingDetermination, complicationRange, actor) {
    let finalDicePool = dicePool + (!!usingDetermination ? -1 : 0);
    const r = new Roll(finalDicePool + 'd20');
    r.roll();
    let _focus = focus || 1;
    let _complication = complicationRange || 20;
    let diceString = '';
    let success = 0;
    let complication = 0;
    let targetText = 'target: ' + target;
    if (focus)
      targetText += ', focus: ' + _focus;
    if (complicationRange)
      targetText += ', complication from: ' + _complication;
    if (usingDetermination)
      targetText += ', Using Determination';
    for (let i = 0; i < finalDicePool; i++) {
      let result = r.terms[0].results[i].result;
      if (result <= _focus) {
        diceString += '<li class="roll die d20 max">' + result + '</li>';
        success += 2;
      }
      else if (result <= target) {
        diceString += '<li class="roll die d20">' + result + '</li>';
        success += 1;
      }
      else if (result >= _complication) {
        diceString += '<li class="roll die d20 min">' + result + '</li>';
        complication += 1;
      }
      else {
        diceString += '<li class="roll die d20">' + result + '</li>';
      }
    }
    // If using a Value and Determination, automatically add in an extra critical roll
    if (usingDetermination) {
      if (actor && actor.system.resources) {
        console.log(actor);
        if (actor.system.resources.determination.value < 1) {
          ui.notifications.error("No determination to spend");
          return false;
        }
        actor.update({
          "system.resources.determination.value": actor.system.resources.determination.value - 1
        });
      }
      diceString += '<li class="roll die d20 max">' + 1 + '</li>';
      success += 2;
    }
    // Here we want to check if the success was exactly one (as "1 Successes" doesn't make grammatical sense).
    // We create a string for the Successes.
    let successText = '';
    if (success <= 1) {
      successText = success + ' ' + game.i18n.format('DUNE.roll.success');
    } else {
      successText = success + ' ' + game.i18n.format('DUNE.roll.successPlural');
    }
    let complicationText = '';
    if (complication > 1) {
      const localisedPluralisation = game.i18n.format('DUNE.roll.complicationPlural');
      complicationText = '<h4 class="dice-total failure"> ' + localisedPluralisation.replace('|#|', complication) + '</h4>';
    } else if (complication) {
      complicationText = '<h4 class="dice-total failure"> ' + game.i18n.format('DUNE.roll.complication') + '</h4>';
    }


    // Build a dynamic html using the variables from above.
    const html = `
            <div class="dune roll attribute">
                <div class="dice-roll">
                    <div class="dice-result">
                        <div class="dice-formula">
                            ` + dicePool + `d20
                        </div>
                        <div class="dice-tooltip">
                            <section class="tooltip-part">
                                <div class="parameters">
                                  ` + targetText + `
                                </div>
                                <div class="dice">
                                    <ol class="dice-rolls">` + diceString + `</ol>
                                </div>
                            </section>
                        </div>` +
      complicationText +
      `<h4 class="dice-total">` + successText + `</h4>
                    </div>
                </div>
            </div>
        `;

    // Check if the dice3d module exists (Dice So Nice). If it does, post a roll in that and then
    // send to chat after the roll has finished. If not just send to chat.
    if (game.dice3d) {
      game.dice3d.showForRoll(r).then((displayed) => {
        this.sendToChat(html, r, actor);
      });
    } else {
      this.sendToChat(html, r, actor);
    };
  }

  async sendToChat(content, roll, actor) {
    let conf = {
      user: game.user._id,
      content: content,
      roll: roll,
      sound: 'sounds/dice.wav'
    };
    if (actor)
      conf.speaker = ChatMessage.getSpeaker({ actor: actor });
    // Send's Chat Message to foundry, if items are missing they will appear as false or undefined and this not be rendered.
    ChatMessage.create(conf).then((msg) => {
      return msg;
    });
  }

  static instance = null;

  static get() {
    if (!DuneRoll.instance)
      DuneRoll.instance = new DuneRoll();
    return DuneRoll.instance;
  }

  // Parse XdYtZfAc || XdYsZfAc
  // {size of dice pool}d{target number}(t|s)[{skill level - for focus}f][{complication range}c][D]
  async parse(cmd, usingDetermination) {
    let actor = game.user.character;
    if (canvas.tokens.controlled.length > 0)
      actor = canvas.tokens.controlled[0].actor;
    let r = cmd.match(/([2-5])d([01]?[0-9])[ts](([4-8])f)?((20|[1][5-9])c)?(D)?/);
    if (r) {
      //console.log(r);
      let dicePool = +r[1];
      let target = +r[2];
      let focus = +r[4];
      if (!!r[7]) usingDetermination = true;
      let complicationRange = +r[6];
      this.performTest(dicePool, target, focus, usingDetermination, complicationRange, actor);
    } else
      ui.notifications.error("Unparsable command: " + cmd);
  }

  static previousValues = {
    dicePool: 2
  };

  static rollerTemplate1 = 'systems/dune/templates/apps/roll.html';
  static rollerTemplate2 = 'systems/dune/templates/apps/roll2.html';

  static async ui() {
    let charData = function () {
      let o = Object.assign({ _template: DuneRoll.rollerTemplate1 }, DuneRoll.previousValues);
      try {
        let actor = game.user.character;
        if (canvas.tokens.controlled.length > 0)
          actor = canvas.tokens.controlled[0].actor;
        if (actor && actor.system.Skills && actor.system.Drives) {
          o.determination = actor.system.resources?.determination?.value || 0;
          o.actor = actor.system;
          o._actor = actor;
          o._template = DuneRoll.rollerTemplate2;
        }
      } catch (e) {
        console.log(e);
        return Object.assign({ _template: DuneRoll.rollerTemplate1 }, DuneRoll.previousValues);
      }
      return o;
    };
    let data = charData();
    let html = await renderTemplate(data._template, data);
    let ui = new Dialog({
      title: game.i18n.localize("DUNE.roll.roller"),
      content: html,
      buttons: {
        roll: {
          label: game.i18n.localize('DUNE.apps.rolldice'),
          callback: (html) => {
            let form = html.find('#dice-pool-form');
            if (!form[0].checkValidity()) {
              throw "Invalid Data";
            }
            let dicePool, target, focus, usingDetermination, complicationRange, skill = 0, drive = 0;
            form.serializeArray().forEach(e => {
              switch (e.name) {
                case "complicationRange":
                  if (e.value != "")
                    complicationRange = +e.value;
                  break;
                case "dicePoolSlider":
                  DuneRoll.previousValues.dicePool = dicePool = +e.value;
                  break;
                case "targetNumber":
                  DuneRoll.previousValues.target = target = +e.value;
                  break;
                case "skill":
                  skill = +e.value;
                  DuneRoll.previousValues.target = target = drive + skill;
                  break;
                case "drive":
                  drive = +e.value;
                  DuneRoll.previousValues.target = target = drive + skill;
                  break;
                case "usingFocus":
                  if (e.value && +e.value > 1)
                    focus = +e.value;
                  break;
                case "usingFocusOn":
                  if (e.value && e.value == "on")
                    focus = skill;
                  break;
                case "usingDetermination":
                  usingDetermination = e.value == "on";
                  break;
              }
            });
            return DuneRoll.get().performTest(dicePool, target, focus, usingDetermination, complicationRange, data._actor);
          }
        },
        close: {
          label: game.i18n.localize('Close'),
          callback: () => { }
        }
      },
      render: function (h) {
        h.find("#skills-radio input").change(function () {
          let s = $(this).attr("data-skill");
          h.find(".focus-list .hidden").removeClass("show");
          let f = h.find(".focus-list ." + s);
          f.addClass("show");
          if (f.length == 0) {
            h.find(".use-focus input").attr("disabled", "disabled").prop("checked", false);
          } else
            h.find(".use-focus input").attr("disabled", null);
        });
      }
    });
    ui.render(true);
    return ui;
  }
}

Handlebars.registerHelper('concat', (...args) => args.slice(0, -1).join(''));
Handlebars.registerHelper('lower', e => e.toLocaleLowerCase());

Hooks.on("chatCommandsReady", function (chatCommands) {
  chatCommands.registerCommand(chatCommands.createCommandFromData({
    commandKey: "/dr",
    invokeOnCommand: (chatlog, messageText, chatdata) => {
      DuneRoll.get().parse(messageText);
    },
    shouldDisplayToChat: false,
    iconClass: "fa-dice-d20",
    description: "Roll Dune check"
  }));
});
