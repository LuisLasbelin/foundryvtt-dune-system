// -*- js -*-
import ItemSheetDuneDomain from "../../item/sheets/domain.js"
import { confirmDelete } from "../utils.js"

class EnemySubSheet extends ItemSheet {
  /** @override */
  get template() {
    return "systems/dune/templates/part/enemy-subsheet.html"
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      closeOnSubmit: false,
      submitOnChange: true,
      popOut: false
    });
  }

  activateListeners(html) {
    super.activateListeners(html);
    if (this.isEditable) {
      html.find("button.del").click(e => confirmDelete(() => {
        this.actor.deleteEmbeddedDocuments("Item", [this.item.id]);
        e.stopPropagation();
      }, this.item));
    }
  }
}

export default class ActorSheetDuneHouse extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      template: "systems/dune/templates/actors/house-sheet.html",
      height: 768,
      tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "main" }]
    });
  }

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   * @override
   */
  activateListeners(html) {
    super.activateListeners(html);
    const softLock = this.actor.system.softLock;
    const editable = this.isEditable && !softLock;
    if (editable) {
      html.find(".add-domain").click(this._addDomain.bind(this));
      html.find(".add-enemy").click(this._addEnemy.bind(this));
      html.find(".domain button.del").click(this._removeDomain.bind(this));
      html.find(".domain[data]").click(this._editDomain.bind(this));
    }
    if (softLock) {
      html.find("input, button, textarea").attr("disabled", true);
    }
    html.find(".soft-lock").click(e => this.actor.update({ "system.softLock": !this.actor.system.softLock }));
  }

  /** @override */
  async _renderInner(...args) {
    const html = await super._renderInner(...args);
    const editable = this.isEditable && !this.actor.system.softLock;
    const enemies = html.find(".enemies ul");
    for (const i of this.actor.items.filter(i => i.type == 'enemy')) {
      let e = new EnemySubSheet(i, { editable: editable });
      await e._render(true);
      enemies.append($(e.form));
    }
    return html;
  }

  static DOMAIN = {
    field: null,
    area: null,
    primary: false,
    details: null,
    edit: true
  };

  static ENEMY = {
    name: null,
    type: null,
    hatered: 1,
    reason: null,
    notes: null
  };

  _editDomain(e) {
    let id = $(e.target).parents(".domain[data]").attr("data");
    let item = this.actor.items.get(id);
    new ItemSheetDuneDomain(item).render(true);
  }

  _addDomain() {
    this.actor.createEmbeddedDocuments("Item", [{ type: "domain", name: "domain" }], { renderSheet: false });
  }

  _removeDomain(e) {
    let id = $(e.target).parents(".domain[data]").attr("data");
    let item = this.actor.items.get(id);
    confirmDelete(() => this.actor.deleteEmbeddedDocuments("Item", [id]), item);
    e.stopPropagation();
  }

  _addEnemy() {
    let nb = this.actor.items.filter(i => i.type == 'enemy').length + 1;
    this.actor.createEmbeddedDocuments("Item", [{ type: "enemy", name: "enemy#" + nb }], { renderSheet: false });
  }

}
